({
    getResponse: function(component,event,helper) {
        var action = component.get("c.getEvents");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log("Data: \n" + result);
                var eventArr = [];
                result.forEach(function(key) {
                    console.log(key);
                    eventArr.push({
                        'id':key.eveId,
                        'start':key.startDT,
                        'end':key.endDT,
                        'title':key.subject
                    });
                });
                this.loadCalendar(component,event,helper,eventArr);
                
            } 
            else if (state === "INCOMPLETE") {}
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    loadCalendar :function(component,event,helper,data){   
        var m = moment();
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: m.format(),
            defaultView: 'agendaWeek',
            navLinks: true, // can click day/week names to navigate views
            weekNumbers: true,
            weekNumbersWithinDays: true,
            weekNumberCalculation: 'ISO',
            selectable: true,
            selectOverlap: false,
            eventLimit: true,
            events:data,
            
            select: function(start, end) {
                console.log(start._d);
                console.log(end._d);
                var createNewEvent = $A.get("e.force:createRecord");
                createNewEvent.setParams({
                    "entityApiName": "Event",
                    "defaultFieldValues": {
                        'WhoId' : component.get("v.recordId"),
                        'StartDateTime' : start._d,
                        'EndDateTime' : end._d
                    }
                });
                createNewEvent.fire();
            },
            eventClick: function(calEvent, jsEvent, view) {
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": calEvent.id
                });
                navEvt.fire();
            }
        });
    }
})