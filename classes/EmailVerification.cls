global with sharing class EmailVerification implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        try{
            Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
            
            List<string> lstEmailBodyText = new List<string>();
            Event eve;
            String sEmail;
            String emailSubject = email.subject;
            String emailBodyText = email.plainTextBody;
            String sEventNumber = emailSubject.substringBetween('[ref:',']'); 
            
            if(emailBodyText != null && string.isNotEmpty(emailBodyText)){
                lstEmailBodyText = emailBodyText.split('\n');
            }
            
            if(!String.isBlank(sEventNumber)){
                eve = [Select Id, WhoId, Event_Number__c, AppointmentConfirmedbyContact__c 
                       from Event WHERE Event_Number__c = :sEventNumber LIMIT 1];
                
                sEmail = [Select Id, Email From Contact WHERE Id =:eve.WhoId].Email;
            }
            
            if(!lstEmailBodyText.isEmpty() && String.isNotBlank(sEmail) && sEmail == email.fromAddress){
                for(Integer i=0;i<lstEmailBodyText.size();i++){
                    String line = lstEmailBodyText[i];
                    if(String.isNotEmpty(line) && (line.containsIgnoreCase('Yes') || line.containsIgnoreCase('No'))){ 
                        eve.AppointmentConfirmedbyContact__c = line;
                        break;
                    } 
                }
            }
            try{
                update eve;    
            }Catch(Exception ex){
                System.debug('ex ' + ex);
            }
            return result;
        }Catch(Exception e){
            return null;
        }
    }
}