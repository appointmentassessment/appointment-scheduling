public class EventCalendarController {
    
    @AuraEnabled
    public static List<EventData> getEvents(){
        List<EventData> result = new List<EventData>();
        
        String tz = UserInfo.getTimeZone().toString();
    
        for(Event e : [Select Id, Subject, StartDateTime, EndDateTime, WhoId from Event]){
            EventData eve = new EventData();
            eve.eveId = e.Id;
            eve.subject = e.Subject;
            eve.startDT = DateTime.valueOfGmt(e.StartDateTime.format('yyyy-MM-dd HH:mm:ss',tz));
            eve.endDT = DateTime.valueOfGmt(e.EndDateTime.format('yyyy-MM-dd HH:mm:ss',tz));
            result.add(eve);
        }
        return result ; 
    }
    
    public class EventData{
        @AuraEnabled 
        public Id eveId;
        @AuraEnabled
        public String subject;
        @AuraEnabled
        public DateTime startDT;
        @AuraEnabled
        public DateTime endDT;
    }
    
    public static void checkDuplicate(List<Event> newList){
        List<Id> eventAssignedToList = new List<Id>();
        
        for(Event e : newList){
            eventAssignedToList.add(e.OwnerId);
        }
        
        if(!eventAssignedToList.isEmpty()){
            for(Event e : [SELECT Id, StartDateTime, ShowAs, EndDateTime, ActivityDate, OwnerId, Who.Name, IsAllDayEvent FROM Event WHERE OwnerId IN :eventAssignedToList]){
                for(Event evt : newList){ 					                  
                    if(evt.OwnerId == e.OwnerId && e.Id != evt.Id){
                        if((e.IsAllDayEvent == true || evt.IsAllDayEvent == true) && evt.StartDateTime.date() == e.StartDateTime.date()){
                            evt.addError('You are currently scheduled for another meeting with ' + e.Who.Name + ' during this time.');
                        }
                        else if(!(evt.StartDateTime >= e.EndDateTime || evt.EndDateTime <= e.StartDateTime)){
                            evt.addError('You are currently scheduled for another meeting with ' + e.Who.Name + ' during this time.');
                        }
                    }
                }
            }
        }
    }
    
    public static void updateEndDate(List<Event> newList){
        List<Event> updatedEvents = new List<Event>();
        
        for(Event e : newList){
            if(e.IsAllDayEvent){
                Event eve = new Event();
                eve.Id = e.Id;
                eve.EndDateTime = e.EndDateTime.addDays(1);
                updatedEvents.add(eve);
            }
        }
        try{
            update updatedEvents;
        }catch(Exception e){}
    }
}