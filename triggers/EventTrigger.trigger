trigger EventTrigger on Event (before insert, before update, after insert){
    
    if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
		EventCalendarController.checkDuplicate(trigger.new);    
    }
    
    if(trigger.isAfter && (trigger.isInsert)){
		EventCalendarController.updateEndDate(trigger.new);    
    }
}